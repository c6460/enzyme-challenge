# EnzymeChallenge - Food Stand

## Problem description

Using the data available at https://data.sfgov.org/resource/rqzj-sfat.json find out which opened japanese food truck is the closest to union square.

## Installation

Install dependencies with
```
mix deps.get
```

## Running the code challenge
Start an interactive session with the project code:
```
iex -S mix
```

To run
```
EnzymeChallenge.main
```
This should return a tuple consisting of the food truck applicant name and address (composite key), and the computed distance from Union Square in San Francisco.

To change the params (maybe to pull the closest food truck serving mexican food) you can run:
```
EnzymeChallenge.process_trucks("https://data.sfgov.org/resource/rqzj-sfat.json", {37.7879, -122.4075}, "Mexican")
```

## Room for improvement

I'm processing the truck data as an array of maps. Ideally I would want to build in more structure/validation around this and cast these maps to a struct, or even an ecto schema/model.

Add typespec info.
