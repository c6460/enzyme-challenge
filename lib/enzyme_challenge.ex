defmodule EnzymeChallenge do
  @doc """
  Get the distance between two sets of coordinates.
  """
  def get_coord_distance({source_x, source_y}, {dest_x, dest_y}) do
    Math.sqrt(Math.pow(source_x - dest_x, 2) + Math.pow(source_y - dest_y, 2))
  end

  @doc """
  Perform a get request to an endpoint and parse the
  json body of the response.
  """
  def fetch_food_truck_data(url) do
    HTTPoison.start()
    resp = HTTPoison.get!(url)
    {:ok, data} = JSON.decode(resp.body)
    data
  end

  @doc """
  Filter food trucks by the `fooditems` field for trucks matching a keyword. Since
  casing may not match, both the search term and the food items is downcased. Only
  trucks with a permit status matching the one of the valid statuses are returned.
  """
  def filter_trucks_by_food_items_and_statuses(data, item_criteria, statuses) do
    data
    |> Enum.filter(fn food_truck_data ->
      Map.get(food_truck_data, "fooditems", "")
      |> String.downcase()
      |> String.contains?(String.downcase(item_criteria)) and
        Enum.any?(statuses, fn status -> Map.get(food_truck_data, "status", "") == status end)
    end)
  end

  @doc """
  Cast a string coordinate to a float. Helpful in
  order to calculate the distance between two points.
  """
  def parse_coordinate(coord) do
    {float_coord, _} = Float.parse(coord)
    float_coord
  end

  @doc """
  Get the distances between the food trucks and a target
  coordinate.
  """
  def get_truck_distances(data, target_coords) do
    Enum.map(data, fn truck ->
      {"#{truck["applicant"]}-#{truck["address"]}",
       get_coord_distance(
         target_coords,
         {parse_coordinate(truck["latitude"]), parse_coordinate(truck["longitude"])}
       )}
    end)
  end

  @doc """
  Given a list of truck distances, get the closest truck to the
  target.
  """
  def find_closest_truck(truck_distances) do
    Enum.min_by(truck_distances, fn {_name, distance} -> distance end)
  end

  @doc """
  Main processing function for the code challenge. Processes the
  food truck data, filters food trucks by offering keyword and
  permit status and then computes the distances of each truck to a given
  tuple of coordinates. Finally the closest truck to those
  coordinates is returned. If no valid truck is found
  a `:no_truck_found` atom is returned.
  """
  def process_trucks(
        food_truck_data,
        target_coords,
        truck_offering,
        valid_statuses \\ ["APPROVED"]
      ) do
    trucks_matching_criteria = food_truck_data
    |> filter_trucks_by_food_items_and_statuses(truck_offering, valid_statuses)
    |> get_truck_distances(target_coords)

    case trucks_matching_criteria do
      [] -> :no_truck_found
      truck_data -> find_closest_truck(truck_data)
    end
  end

  @doc """
  Entry point for the code challenge, populates each param
  for `process_trucks`.
  """
  def main do
    food_truck_data = "https://data.sfgov.org/resource/rqzj-sfat.json" |> fetch_food_truck_data()
    union_square = {37.7879, -122.4075}
    truck_offering = "Japanese"

    process_trucks(food_truck_data, union_square, truck_offering)
  end
end
