defmodule EnzymeChallengeTest do
  use ExUnit.Case
  doctest EnzymeChallenge

  test "test coordinate distance" do
    assert EnzymeChallenge.get_coord_distance({0.1, 0.2}, {0.2, 0.1}) == 0.14142135623730953
  end

  test "test filter truck by food items" do
    assert EnzymeChallenge.filter_trucks_by_food_items_and_statuses(
             [
               %{"fooditems" => "Japanese bagels", "status" => "APPROVED"},
               %{"fooditems" => "Coffee", "status" => "APPROVED"}
             ],
             "Japanese",
             ["APPROVED"]
           ) == [%{"fooditems" => "Japanese bagels", "status" => "APPROVED"}]
  end

  test "test filter truck by with no matching statuses" do
    assert EnzymeChallenge.filter_trucks_by_food_items_and_statuses(
             [
               %{"fooditems" => "Japanese bagels", "status" => "EXPIRED"},
               %{"fooditems" => "Coffee", "status" => "APPROVED"}
             ],
             "Japanese",
             ["APPROVED", "REQUESTED"]
           ) == []
  end

  test "test filter truck by food items empty result" do
    assert EnzymeChallenge.filter_trucks_by_food_items_and_statuses(
             [%{"fooditems" => "Japanese bagels"}, %{"fooditems" => "Coffee"}],
             "German",
             ["APPROVED"]
           ) == []
  end

  test "test get distances" do
    assert EnzymeChallenge.get_truck_distances(
             [
               %{
                 "applicant" => "app-1",
                 "address" => "123 street",
                 "longitude" => "100.200",
                 "latitude" => "200.02"
               },
               %{
                 "applicant" => "app-2",
                 "address" => "456 street",
                 "longitude" => "500.10",
                 "latitude" => "700.10"
               }
             ],
             {0, 0}
           ) == [
             {"app-1-123 street", 223.71419355955047},
             {"app-2-456 street", 860.3720241848871}
           ]
  end

  test "test closest truck" do
    assert EnzymeChallenge.find_closest_truck([
             {"app-1-123 street", 223.71419355955047},
             {"app-2-456 street", 860.3720241848871}
           ]) == {"app-1-123 street", 223.71419355955047}
  end

  test "test main processing function closest result" do
    truck_data = [
      %{
        "applicant" => "app-1",
        "address" => "123 street",
        "longitude" => "100.200",
        "latitude" => "200.02",
        "status" => "APPROVED",
        "fooditems" => "Hot dogs and ice cream"
      },
      %{
        "applicant" => "app-2",
        "address" => "456 street",
        "longitude" => "500.10",
        "latitude" => "700.10",
        "status" => "REVOKED"
      }
    ]

    assert EnzymeChallenge.process_trucks(truck_data, {0, 0}, "Ice cream") ==
             {"app-1-123 street", 223.71419355955047}
  end

  test "test main processing function no result" do
    truck_data = [
      %{
        "applicant" => "app-1",
        "address" => "123 street",
        "longitude" => "100.200",
        "latitude" => "200.02",
        "status" => "REVOKED",
        "fooditems" => "Hot dogs and ice cream"
      },
      %{
        "applicant" => "app-2",
        "address" => "456 street",
        "longitude" => "500.10",
        "latitude" => "700.10",
        "status" => "REVOKED"
      }
    ]

    assert EnzymeChallenge.process_trucks(truck_data, {0, 0}, "Ice cream") ==
             :no_truck_found
  end
end
